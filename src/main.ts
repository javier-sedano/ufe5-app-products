import { createApp } from 'vue';
import App from './App.vue';
import { createPinia } from 'pinia';
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import { setSingletonBaseUrl } from 'javier-sedano-ufe5-lib-login';
import router from './router';

const app = createApp(App);

app.use(createPinia().use(piniaPluginPersistedstate));
app.use(router);

const baseUrl = import.meta.env.VITE_BASE_URL || `http://localhost:4103`;
setSingletonBaseUrl(baseUrl);

app.mount('#app');
